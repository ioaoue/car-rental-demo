FROM gradle:5.2-jdk11-slim AS build

COPY --chown=gradle . /project
WORKDIR /project
RUN gradle build --stacktrace


FROM openjdk:11-slim

COPY --from=build /project/build/libs/car-rental-demo.jar /usr/lib/car-rental-demo/car-rental-demo.jar

RUN useradd -r demo
USER demo

USER demo
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/lib/car-rental-demo/car-rental-demo.jar"]

EXPOSE 8080