package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Manufacturer;
import com.example.carrentaldemo.model.QManufacturer;
import com.example.carrentaldemo.repository.ManufacturerRepository;
import com.example.carrentaldemo.service.exception.ManufacturerNotFoundException;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    private ManufacturerRepository manufacturerRepository;

    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Transactional
    @Override
    public Manufacturer registerManufacturer(String name) {
        Manufacturer manufacturer = new Manufacturer(UUID.randomUUID(), name);
        return manufacturerRepository.save(manufacturer);
    }

    @Transactional(readOnly = true)
    @Override
    public Manufacturer findActiveManufacturer(UUID manufacturerId) throws ManufacturerNotFoundException {
        Predicate predicate = manufacturerIdEquals(manufacturerId).and(manufacturerIsActive());
        return manufacturerRepository.findOne(predicate)
                .orElseThrow(() -> new ManufacturerNotFoundException(manufacturerId));
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<Manufacturer> findActiveManufacturers() {
        return manufacturerRepository.findAll(manufacturerIsActive());
    }

    private BooleanExpression manufacturerIdEquals(UUID manufacturerId) {
        return QManufacturer.manufacturer.id.eq(manufacturerId);
    }

    private BooleanExpression manufacturerIsActive() {
        return QManufacturer.manufacturer.active.isTrue();
    }

}
