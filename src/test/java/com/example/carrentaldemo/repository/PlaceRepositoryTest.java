package com.example.carrentaldemo.repository;

import com.example.carrentaldemo.model.Place;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PlaceRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PlaceRepository placeRepository;

    @Test
    public void should_persist_and_read() {
        Place place = new Place(UUID.randomUUID(), "Nowhere");
        place = entityManager.persist(place);
        Assert.assertTrue(placeRepository.findById(place.getId()).isPresent());
    }

}