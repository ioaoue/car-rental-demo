package com.example.carrentaldemo.service.exception;

import java.util.UUID;

public class RentNotFoundException extends Exception {

    private UUID rentId;

    public RentNotFoundException(UUID rentId) {
        super("Rent was not found: " + rentId);
        this.rentId = rentId;
    }

    public UUID getRentId() {
        return rentId;
    }

}