package com.example.carrentaldemo.service.exception;

import java.util.UUID;

public class CarNotAvailableException extends Exception {

    private UUID carId;

    public CarNotAvailableException(UUID carId) {
        super("Car is not available: " + carId);
        this.carId = carId;
    }

    public UUID getCarId() {
        return carId;
    }

}