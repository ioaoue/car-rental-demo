package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Car;
import com.example.carrentaldemo.service.exception.CarNotFoundException;
import com.example.carrentaldemo.service.exception.ModelNotFoundException;
import com.example.carrentaldemo.service.exception.PlaceNotFoundException;

import java.util.UUID;

public interface CarService {

    Car registerCar(String number, UUID modelId, UUID placeId) throws ModelNotFoundException, PlaceNotFoundException;

    Car findActiveCar(UUID carId) throws CarNotFoundException;

    Iterable<Car> findActiveCars();

}
