package com.example.carrentaldemo.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Model {
    @Id
    @NotNull
    private UUID id;

    @NotBlank
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private Manufacturer manufacturer;

    private boolean active;

    @Version
    private long version;

    public Model() {
    }

    public Model(@NotNull UUID id, @NotNull String name, @NotNull Manufacturer manufacturer) {
        this.id = id;
        this.name = name;
        this.manufacturer = manufacturer;
        this.active = true;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Model)) return false;

        Model model = (Model) o;

        return Objects.equals(id, model.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
