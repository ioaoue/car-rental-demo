package com.example.carrentaldemo.controller;

import com.example.carrentaldemo.controller.dto.DtoMapper;
import com.example.carrentaldemo.controller.dto.ManufacturerDto;
import com.example.carrentaldemo.controller.dto.RegisterManufacturerCommand;
import com.example.carrentaldemo.controller.dto.Response;
import com.example.carrentaldemo.controller.exception.ErrorCode;
import com.example.carrentaldemo.controller.exception.RestException;
import com.example.carrentaldemo.model.Manufacturer;
import com.example.carrentaldemo.service.ManufacturerService;
import com.example.carrentaldemo.service.exception.ManufacturerNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/rest/v1/manufacturers")
public class ManufacturerController {

    private ManufacturerService manufacturerService;
    private DtoMapper dtoMapper;

    public ManufacturerController(ManufacturerService manufacturerService, DtoMapper dtoMapper) {
        this.manufacturerService = manufacturerService;
        this.dtoMapper = dtoMapper;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Response<ManufacturerDto>> registerManufacturer(
            @RequestBody @Valid RegisterManufacturerCommand cmd,
            UriComponentsBuilder uriBuilder
    ) {
        Manufacturer manufacturer = manufacturerService.registerManufacturer(cmd.getName());
        ManufacturerDto manufacturerDto = dtoMapper.mapManufacturer(manufacturer);

        URI location = uriBuilder.path("/rest/v1/manufacturers/{id}")
                .build(manufacturerDto.getId());

        return ResponseEntity.created(location)
                .body(Response.success(manufacturerDto));
    }

    @GetMapping(value = "/{manufacturerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<ManufacturerDto> getManufacturer(@PathVariable UUID manufacturerId) {
        Manufacturer manufacturer;
        try {
            manufacturer = manufacturerService.findActiveManufacturer(manufacturerId);
        } catch (ManufacturerNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.manufacturer.notFound", e.getManufacturerId());
        }
        ManufacturerDto manufacturerDto = dtoMapper.mapManufacturer(manufacturer);
        return Response.success(manufacturerDto);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Iterable<ManufacturerDto>> getManufacturers() {
        Iterable<Manufacturer> manufacturers = manufacturerService.findActiveManufacturers();
        return Response.success(dtoMapper.mapManufacturers(manufacturers));
    }

}
