package com.example.carrentaldemo.controller.dto;

public class Response<T> {

    private T value;

    private Response(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public static <T> Response<T> success(T value) {
        return new Response<>(value);
    }

}
