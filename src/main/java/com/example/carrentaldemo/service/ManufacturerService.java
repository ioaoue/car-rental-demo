package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Manufacturer;
import com.example.carrentaldemo.service.exception.ManufacturerNotFoundException;

import java.util.UUID;

public interface ManufacturerService {

    Manufacturer registerManufacturer(String name);

    Manufacturer findActiveManufacturer(UUID manufacturerId) throws ManufacturerNotFoundException;

    Iterable<Manufacturer> findActiveManufacturers();

}
