package com.example.carrentaldemo.controller;

import com.example.carrentaldemo.controller.dto.*;
import com.example.carrentaldemo.controller.exception.ErrorCode;
import com.example.carrentaldemo.controller.exception.RestException;
import com.example.carrentaldemo.model.Rent;
import com.example.carrentaldemo.service.RentService;
import com.example.carrentaldemo.service.exception.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/rest/v1/rent")
public class RentController {

    private RentService rentService;
    private DtoMapper dtoMapper;

    public RentController(RentService rentService, DtoMapper dtoMapper) {
        this.rentService = rentService;
        this.dtoMapper = dtoMapper;
    }

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Response<RentDto>> registerRent(@RequestBody @Valid RentCarCommand cmd) {
        Rent rent;
        try {
            rent = rentService.registerCarRented(cmd.getCarId(), cmd.getClientId());
        } catch (CarNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.car.notFound", e.getCarId());
        } catch (CarNotAvailableException e) {
            throw new RestException(ErrorCode.NOT_AVAILABLE, "error.car.notAvailable", e.getCarId());
        } catch (ClientNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.client.notFound", e.getClientId());
        }
        RentDto rentDto = dtoMapper.mapRent(rent);

        return ResponseEntity.ok(Response.success(rentDto));
    }


    @PostMapping(value = "/return", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Response<RentDto>> registerReturn(@RequestBody @Valid ReturnCarCommand cmd) {
        Rent rent;
        try {
            rent = rentService.registerCarReturned(cmd.getCarId(), cmd.getPlaceId());
        } catch (CarNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.car.notFound", e.getCarId());
        } catch (PlaceNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.place.notFound", e.getPlaceId());
        } catch (CarNotRentedException e) {
            throw new RestException(ErrorCode.NOT_AVAILABLE, "error.car.notRented", e.getCarId());
        }
        RentDto rentDto = dtoMapper.mapRent(rent);

        return ResponseEntity.ok(Response.success(rentDto));
    }

}
