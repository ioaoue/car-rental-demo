package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Client;
import com.example.carrentaldemo.service.exception.ClientNotFoundException;

import java.util.UUID;

public interface ClientService {

    Client registerClient(String fullName);

    Client findActiveClient(UUID clientId) throws ClientNotFoundException;

    Iterable<Client> findActiveClients();

}
