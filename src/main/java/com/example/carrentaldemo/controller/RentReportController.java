package com.example.carrentaldemo.controller;

import com.example.carrentaldemo.controller.dto.*;
import com.example.carrentaldemo.model.Manufacturer;
import com.example.carrentaldemo.model.Place;
import com.example.carrentaldemo.model.Rent;
import com.example.carrentaldemo.service.RentReportService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.*;

@RestController
@RequestMapping("/rest/v1/report/rent")
public class RentReportController {

    private RentReportService rentReportService;
    private DtoMapper dtoMapper;

    public RentReportController(RentReportService rentReportService, DtoMapper dtoMapper) {
        this.rentReportService = rentReportService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping(value = "/history", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Iterable<RentHistoryDto>> getHistory(
            @RequestParam(required = false) UUID manufacturerId,
            @RequestParam(required = false) Instant rentTimestampMin,
            @RequestParam(required = false) Instant rentTimestampMax,
            @RequestParam(required = false) Instant returnTimestampMin,
            @RequestParam(required = false) Instant returnTimestampMax,
            @RequestParam(required = false) String clientFullName,
            @RequestParam(required = false) String carNumber
    ) {
        Iterable<Rent> history = rentReportService.getHistory(
                manufacturerId,
                rentTimestampMin,
                rentTimestampMax,
                returnTimestampMin,
                returnTimestampMax,
                clientFullName,
                carNumber
        );
        return Response.success(dtoMapper.mapRentHistory(history));
    }

    @GetMapping(value = "/average", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<AverageRentDurationDto> getAverageDuration(
            @RequestParam(required = false) Instant rentTimestampMin,
            @RequestParam(required = false) Instant rentTimestampMax
    ) {
        Iterable<RentReportService.AverageRentDuration> rentDurations = rentReportService
                .getAverageRentDuration(rentTimestampMin, rentTimestampMax);

        Map<UUID, ManufacturerDto> manufacturers = new HashMap<>();
        Map<UUID, PlaceDto> places = new HashMap<>();
        List<AverageRentDurationDto.AverageRecordDto> average = new ArrayList<>();

        AverageRentDurationDto result = new AverageRentDurationDto();
        for (RentReportService.AverageRentDuration rentDuration: rentDurations) {
            AverageRentDurationDto.AverageRecordDto record = new AverageRentDurationDto.AverageRecordDto();
            Manufacturer manufacturer = rentDuration.getManufacturer();
            Place place = rentDuration.getPlace();

            record.setManufacturerId(manufacturer.getId());
            record.setPlaceId(place.getId());
            record.setDuration(rentDuration.getDuration());

            if (!manufacturers.containsKey(manufacturer.getId())) {
                manufacturers.put(manufacturer.getId(), dtoMapper.mapManufacturer(manufacturer));
            }
            if (!places.containsKey(place.getId())) {
                places.put(place.getId(), dtoMapper.mapPlace(place));
            }
            average.add(record);
        }
        result.setAverage(average);
        result.setManufacturers(manufacturers);
        result.setPlaces(places);
        return Response.success(result);
    }

}
