package com.example.carrentaldemo.controller;

import com.example.carrentaldemo.controller.dto.ClientDto;
import com.example.carrentaldemo.controller.dto.DtoMapper;
import com.example.carrentaldemo.controller.dto.RegisterClientCommand;
import com.example.carrentaldemo.controller.dto.Response;
import com.example.carrentaldemo.controller.exception.ErrorCode;
import com.example.carrentaldemo.controller.exception.RestException;
import com.example.carrentaldemo.model.Client;
import com.example.carrentaldemo.service.ClientService;
import com.example.carrentaldemo.service.exception.ClientNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/rest/v1/clients")
public class ClientController {

    private ClientService clientService;
    private DtoMapper dtoMapper;

    public ClientController(ClientService clientService, DtoMapper dtoMapper) {
        this.clientService = clientService;
        this.dtoMapper = dtoMapper;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Response<ClientDto>> registerClient(
            @RequestBody @Valid RegisterClientCommand cmd,
            UriComponentsBuilder uriBuilder
    ) {
        Client client = clientService.registerClient(cmd.getFullName());
        ClientDto clientDto = dtoMapper.mapClient(client);

        URI location = uriBuilder.path("/rest/v1/clients/{id}")
                .build(clientDto.getId());

        return ResponseEntity.created(location)
                .body(Response.success(clientDto));
    }

    @GetMapping(value = "/{clientId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<ClientDto> getClient(@PathVariable UUID clientId) {
        Client client;
        try {
            client = clientService.findActiveClient(clientId);
        } catch (ClientNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.client.notFound", e.getClientId());
        }
        ClientDto clientDto = dtoMapper.mapClient(client);
        return Response.success(clientDto);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Iterable<ClientDto>> getClients() {
        Iterable<Client> clients = clientService.findActiveClients();
        return Response.success(dtoMapper.mapClients(clients));
    }

}
