package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.*;
import com.example.carrentaldemo.repository.RentRepository;
import com.example.carrentaldemo.service.exception.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.UUID;

@Service
public class RentServiceImpl implements RentService {

    private RentRepository rentRepository;
    private CarService carService;
    private ClientService clientService;
    private PlaceService placeService;
    private TimeService timeService;

    public RentServiceImpl(
            RentRepository rentRepository,
            CarService carService,
            ClientService clientService,
            PlaceService placeService,
            TimeService timeService
    ) {
        this.rentRepository = rentRepository;
        this.carService = carService;
        this.clientService = clientService;
        this.placeService = placeService;
        this.timeService = timeService;
    }

    @Transactional
    @Override
    public Rent registerCarRented(UUID carId, UUID clientId) throws CarNotFoundException, CarNotAvailableException,
            ClientNotFoundException {
        Car car = carService.findActiveCar(carId);
        Client client = clientService.findActiveClient(clientId);
        return registerCarRented(car, client);
    }

    private Rent registerCarRented(Car car, Client client) throws CarNotAvailableException {
        if (car.getCurrentRent() != null) {
            throw new CarNotAvailableException(car.getId());
        }
        Instant now = timeService.now();
        Rent rent = new Rent(UUID.randomUUID(), now, car.getCurrentPlace(), car, client);
        rent = rentRepository.save(rent);
        car.setCurrentRent(rent);
        car.setCurrentPlace(null);
        return rent;
    }

    @Transactional
    @Override
    public Rent registerCarReturned(UUID carId, UUID placeId) throws CarNotFoundException, PlaceNotFoundException,
            CarNotRentedException {
        Car car = carService.findActiveCar(carId);
        Place place = placeService.findActivePlace(placeId);
        return registerCarReturned(car, place);
    }

    private Rent registerCarReturned(Car car, Place place) throws CarNotRentedException {
        Instant now = timeService.now();
        Rent rent = car.getCurrentRent();
        if (rent == null) {
            throw new CarNotRentedException(car.getId());
        }
        rent.setReturnTimestamp(now);
        rent.setReturnPlace(place);
        rent.setStatus(RentStatus.COMPLETED);
        car.setCurrentPlace(place);
        car.setCurrentRent(null);
        return rent;
    }

}
