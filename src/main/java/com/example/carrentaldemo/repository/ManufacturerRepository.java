package com.example.carrentaldemo.repository;

import com.example.carrentaldemo.model.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, UUID>,
        QuerydslPredicateExecutor<Manufacturer> {
}
