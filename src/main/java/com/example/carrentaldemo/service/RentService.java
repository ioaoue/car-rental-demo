package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Rent;
import com.example.carrentaldemo.service.exception.*;

import java.util.UUID;

public interface RentService {

    Rent registerCarRented(UUID carId, UUID clientId) throws CarNotFoundException, CarNotAvailableException,
            ClientNotFoundException;

    Rent registerCarReturned(UUID carId, UUID placeId) throws CarNotFoundException, PlaceNotFoundException,
            CarNotRentedException;

}
