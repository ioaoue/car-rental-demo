package com.example.carrentaldemo.service.exception;

import java.util.UUID;

public class ClientNotFoundException extends Exception {

    private UUID clientId;

    public ClientNotFoundException(UUID clientId) {
        super("Client was not found: " + clientId);
        this.clientId = clientId;
    }

    public UUID getClientId() {
        return clientId;
    }

}