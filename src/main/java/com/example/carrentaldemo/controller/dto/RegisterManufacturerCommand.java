package com.example.carrentaldemo.controller.dto;

import javax.validation.constraints.NotBlank;

public class RegisterManufacturerCommand {

    @NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
