package com.example.carrentaldemo.controller.dto;

import java.util.UUID;

public class ClientDto {

    private UUID id;

    private String fullName;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

}
