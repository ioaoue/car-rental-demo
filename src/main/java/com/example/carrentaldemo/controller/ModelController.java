package com.example.carrentaldemo.controller;

import com.example.carrentaldemo.controller.dto.DtoMapper;
import com.example.carrentaldemo.controller.dto.ModelDto;
import com.example.carrentaldemo.controller.dto.RegisterModelCommand;
import com.example.carrentaldemo.controller.dto.Response;
import com.example.carrentaldemo.controller.exception.ErrorCode;
import com.example.carrentaldemo.controller.exception.RestException;
import com.example.carrentaldemo.model.Model;
import com.example.carrentaldemo.service.ModelService;
import com.example.carrentaldemo.service.exception.ManufacturerNotFoundException;
import com.example.carrentaldemo.service.exception.ModelNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/rest/v1/models")
public class ModelController {

    private ModelService modelService;
    private DtoMapper dtoMapper;

    public ModelController(ModelService modelService, DtoMapper dtoMapper) {
        this.modelService = modelService;
        this.dtoMapper = dtoMapper;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Response<ModelDto>> registerModel(
            @RequestBody @Valid RegisterModelCommand cmd,
            UriComponentsBuilder uriBuilder
    ) {
        Model model;
        try {
            model = modelService.registerModel(cmd.getName(), cmd.getManufacturerId());
        } catch (ManufacturerNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.manufacturer.notFound", e.getManufacturerId());
        }
        ModelDto modelDto = dtoMapper.mapModel(model);

        URI location = uriBuilder.path("/rest/v1/models/{id}")
                .build(modelDto.getId());

        return ResponseEntity.created(location)
                .body(Response.success(modelDto));
    }

    @GetMapping(value = "/{modelId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<ModelDto> getModel(@PathVariable UUID modelId) {
        Model model;
        try {
            model = modelService.findActiveModel(modelId);
        } catch (ModelNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.model.notFound", e.getModelId());
        }
        ModelDto modelDto = dtoMapper.mapModel(model);
        return Response.success(modelDto);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Iterable<ModelDto>> getModels() {
        Iterable<Model> models = modelService.findActiveModels();
        return Response.success(dtoMapper.mapModels(models));
    }

}
