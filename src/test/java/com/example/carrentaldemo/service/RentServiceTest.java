package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.*;
import com.example.carrentaldemo.repository.RentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RentServiceTest {

    @TestConfiguration
    static class RentServiceTestConfiguration {

        @MockBean
        private RentRepository rentRepository;

        @MockBean
        private CarService carService;

        @MockBean
        private ClientService clientService;

        @MockBean
        private PlaceService placeService;

        @MockBean
        private TimeService timeService;

        @Bean
        TimeService timeService() {
            return new TimeServiceImpl();
        }

        @Bean
        RentService rentService(TimeService timeService) {
            return new RentServiceImpl(rentRepository, carService, clientService, placeService, timeService);
        }

    }

    @Autowired
    private RentService rentService;

    @Autowired
    private CarService carService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private PlaceService placeService;

    @Autowired
    private RentRepository rentRepository;

    @Test
    public void should_register_rent() throws Exception {
        Manufacturer manufacturer = new Manufacturer(UUID.randomUUID(), "manufacturer");
        Model model = new Model(UUID.randomUUID(), "model", manufacturer);
        Place place = new Place(UUID.randomUUID(), "place");
        Car car = new Car(UUID.randomUUID(), model, place, "number");
        Client client = new Client(UUID.randomUUID(), "client");

        when(carService.findActiveCar(car.getId())).thenReturn(car);
        when(clientService.findActiveClient(client.getId())).thenReturn(client);
        when(rentRepository.save(any())).thenAnswer(invocation -> invocation.getArguments()[0]);

        // TODO: check rent and return timestamps

        Rent rent = rentService.registerCarRented(car.getId(), client.getId());
        assertEquals(rent.getCar(), car);
        assertEquals(rent.getRentPlace(), place);
        assertNull(rent.getReturnPlace());
        assertEquals(rent.getClient(), client);
        assertEquals(rent.getStatus(), RentStatus.ACTIVE);
        assertEquals(car.getCurrentRent(), rent);
        assertNull(car.getCurrentPlace());
    }

    @Test
    public void should_complete_rent() throws Exception {
        Manufacturer manufacturer = new Manufacturer(UUID.randomUUID(), "manufacturer");
        Model model = new Model(UUID.randomUUID(), "model", manufacturer);
        Place oldPlace = new Place(UUID.randomUUID(), "oldPlace");
        Place newPlace = new Place(UUID.randomUUID(), "newPlace");
        Car car = new Car(UUID.randomUUID(), model, oldPlace, "number");
        Client client = new Client(UUID.randomUUID(), "client");
        Rent rent = new Rent(UUID.randomUUID(), Instant.now(), oldPlace, car, client);

        car.setCurrentPlace(null);
        car.setCurrentRent(rent);

        when(carService.findActiveCar(car.getId())).thenReturn(car);
        when(placeService.findActivePlace(newPlace.getId())).thenReturn(newPlace);

        rent = rentService.registerCarReturned(car.getId(), newPlace.getId());

        // TODO: check rent and return timestamps

        assertEquals(rent.getCar(), car);
        assertEquals(rent.getRentPlace(), oldPlace);
        assertEquals(rent.getReturnPlace(), newPlace);
        assertEquals(rent.getClient(), client);
        assertEquals(rent.getStatus(), RentStatus.COMPLETED);
        assertNull(car.getCurrentRent());
        assertEquals(car.getCurrentPlace(), newPlace);
    }

}