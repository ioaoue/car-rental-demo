package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Model;
import com.example.carrentaldemo.service.exception.ManufacturerNotFoundException;
import com.example.carrentaldemo.service.exception.ModelNotFoundException;

import java.util.UUID;

public interface ModelService {

    Model registerModel(String name, UUID manufacturerId) throws ManufacturerNotFoundException;

    Model findActiveModel(UUID modelId) throws ModelNotFoundException;

    Iterable<Model> findActiveModels();

}
