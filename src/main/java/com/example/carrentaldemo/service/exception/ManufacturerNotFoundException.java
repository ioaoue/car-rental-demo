package com.example.carrentaldemo.service.exception;

import java.util.UUID;

public class ManufacturerNotFoundException extends Exception {

    private UUID manufacturerId;

    public ManufacturerNotFoundException(UUID manufacturerId) {
        super("Manufacturer was not found: " + manufacturerId);
        this.manufacturerId = manufacturerId;
    }

    public UUID getManufacturerId() {
        return manufacturerId;
    }

}