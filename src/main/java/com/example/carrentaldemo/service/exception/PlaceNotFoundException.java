package com.example.carrentaldemo.service.exception;

import java.util.UUID;

public class PlaceNotFoundException extends Exception {

    private UUID placeId;

    public PlaceNotFoundException(UUID placeId) {
        super("PlaceDto was not found: " + placeId);
        this.placeId = placeId;
    }

    public UUID getPlaceId() {
        return placeId;
    }

}