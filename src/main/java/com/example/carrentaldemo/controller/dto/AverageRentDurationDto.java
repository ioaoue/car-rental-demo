package com.example.carrentaldemo.controller.dto;

import java.time.Duration;
import java.util.Map;
import java.util.UUID;

public class AverageRentDurationDto {

    private Iterable<AverageRecordDto> average;
    private Map<UUID, ManufacturerDto> manufacturers;
    private Map<UUID, PlaceDto> places;

    public Iterable<AverageRecordDto> getAverage() {
        return average;
    }

    public void setAverage(Iterable<AverageRecordDto> average) {
        this.average = average;
    }

    public Map<UUID, ManufacturerDto> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(Map<UUID, ManufacturerDto> manufacturers) {
        this.manufacturers = manufacturers;
    }

    public Map<UUID, PlaceDto> getPlaces() {
        return places;
    }

    public void setPlaces(Map<UUID, PlaceDto> places) {
        this.places = places;
    }

    public static class AverageRecordDto {

        private UUID manufacturerId;
        private UUID placeId;
        private Duration duration;

        public UUID getManufacturerId() {
            return manufacturerId;
        }

        public void setManufacturerId(UUID manufacturerId) {
            this.manufacturerId = manufacturerId;
        }

        public UUID getPlaceId() {
            return placeId;
        }

        public void setPlaceId(UUID placeId) {
            this.placeId = placeId;
        }

        public Duration getDuration() {
            return duration;
        }

        public void setDuration(Duration duration) {
            this.duration = duration;
        }

    }

}
