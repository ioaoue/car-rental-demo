package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Manufacturer;
import com.example.carrentaldemo.model.Place;
import com.example.carrentaldemo.model.Rent;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

public interface RentReportService {

    Iterable<AverageRentDuration> getAverageRentDuration(Instant rentTimestampMin, Instant rentTimestampMax);

    class AverageRentDuration {

        private Manufacturer manufacturer;
        private Place place;
        private Duration duration;

        public AverageRentDuration(Manufacturer manufacturer, Place place, Duration duration) {
            this.manufacturer = manufacturer;
            this.place = place;
            this.duration = duration;
        }

        public Manufacturer getManufacturer() {
            return manufacturer;
        }

        public Place getPlace() {
            return place;
        }

        public Duration getDuration() {
            return duration;
        }

    }

    Iterable<Rent> getHistory(
            UUID manufacturerId,
            Instant rentTimestampMin,
            Instant rentTimestampMax,
            Instant returnTimestampMin,
            Instant returnTimestampMax,
            String clientFullName,
            String carNumber
    );

}
