CREATE FUNCTION timestamp_to_milliseconds(timestamp) RETURNS bigint
    AS 'SELECT (EXTRACT(EPOCH FROM $1) * 1000)::bigint;'
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;