package com.example.carrentaldemo.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Car {
    @Id
    @NotNull
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private Model model;

    @ManyToOne(fetch = FetchType.LAZY)
    private Place currentPlace;

    @ManyToOne(fetch = FetchType.LAZY)
    private Rent currentRent;

    @NotNull
    private String number;

    private boolean active;

    @Version
    private long version;

    public Car() {
    }

    public Car(@NotNull UUID id, @NotNull Model model, @NotNull Place currentPlace, @NotNull String number) {
        this.id = id;
        this.model = model;
        this.currentPlace = currentPlace;
        this.number = number;
        this.active = true;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Place getCurrentPlace() {
        return currentPlace;
    }

    public void setCurrentPlace(Place currentPlace) {
        this.currentPlace = currentPlace;
    }

    public Rent getCurrentRent() {
        return currentRent;
    }

    public void setCurrentRent(Rent currentRent) {
        this.currentRent = currentRent;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;

        Car car = (Car) o;

        return Objects.equals(id, car.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
