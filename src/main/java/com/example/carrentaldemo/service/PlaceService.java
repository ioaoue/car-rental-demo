package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Place;
import com.example.carrentaldemo.service.exception.PlaceNotFoundException;

import java.util.UUID;

public interface PlaceService {

    Place registerPlace(String address);

    Place findActivePlace(UUID placeId) throws PlaceNotFoundException;

    Iterable<Place> findActivePlaces();

}
