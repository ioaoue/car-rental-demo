package com.example.carrentaldemo.configuration;

import com.example.carrentaldemo.controller.serializer.DurationSerializer;
import com.example.carrentaldemo.controller.serializer.InstantSerializer;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonConfiguration {

    @Bean
    Module dateTimeModule() {
        SimpleModule module = new SimpleModule();
        module.addSerializer(new InstantSerializer());
        module.addSerializer(new DurationSerializer());
        return module;
    }

}
