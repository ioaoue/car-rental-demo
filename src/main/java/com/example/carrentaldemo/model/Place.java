package com.example.carrentaldemo.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Place {
    @Id
    @NotNull
    private UUID id;

    @NotBlank
    private String address;

    private boolean active;

    @Version
    private long version;

    public Place() {
    }

    public Place(@NotNull UUID id, @NotNull String address) {
        this.id = id;
        this.address = address;
        this.active = true;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Place)) return false;

        Place place = (Place) o;

        return Objects.equals(id, place.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
