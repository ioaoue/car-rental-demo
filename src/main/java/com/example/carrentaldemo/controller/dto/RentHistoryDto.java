package com.example.carrentaldemo.controller.dto;

import java.time.Instant;
import java.util.UUID;

public class RentHistoryDto {

    private UUID id;
    private Instant rentTimestamp;
    private Instant returnTimestamp;
    private String clientFullName;
    private String carNumber;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Instant getRentTimestamp() {
        return rentTimestamp;
    }

    public void setRentTimestamp(Instant rentTimestamp) {
        this.rentTimestamp = rentTimestamp;
    }

    public Instant getReturnTimestamp() {
        return returnTimestamp;
    }

    public void setReturnTimestamp(Instant returnTimestamp) {
        this.returnTimestamp = returnTimestamp;
    }

    public String getClientFullName() {
        return clientFullName;
    }

    public void setClientFullName(String clientFullName) {
        this.clientFullName = clientFullName;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

}
