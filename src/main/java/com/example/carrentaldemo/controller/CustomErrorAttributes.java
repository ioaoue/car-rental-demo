package com.example.carrentaldemo.controller;

import com.example.carrentaldemo.controller.exception.ErrorCode;
import com.example.carrentaldemo.controller.exception.RestException;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.ServletException;
import java.util.HashMap;
import java.util.Map;

@Component
public class CustomErrorAttributes extends DefaultErrorAttributes {

    private MessageSourceAccessor messageSourceAccessor;

    public CustomErrorAttributes(MessageSourceAccessor messageSourceAccessor) {
        this.messageSourceAccessor = messageSourceAccessor;
    }

    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        Error error = new Error();
        Throwable exception = getError(webRequest);
        while (exception instanceof ServletException) {
            exception = exception.getCause();
        }
        if (exception instanceof RestException) {
            RestException restException = (RestException) exception;
            error.errorCode = restException.getErrorCode();
            error.message = translateErrorMessage(restException.getMessage(), restException.getArguments());
        } else {
            Integer statusCode = getAttribute(webRequest, "javax.servlet.error.status_code");
            HttpStatus status = HttpStatus.valueOf(statusCode);
            // TODO: more granular error handling
            if (status.is4xxClientError()) {
                error.errorCode = ErrorCode.CLIENT;
                error.message = translateErrorMessage("error.rest.client", new Object[]{});
            } else {
                error.errorCode = ErrorCode.SERVER;
                error.message = translateErrorMessage("error.rest.server", new Object[]{});
            }
        }

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("error", error);
        return attributes;
    }

    @SuppressWarnings("unchecked")
    private <T> T getAttribute(RequestAttributes requestAttributes, String name) {
        return (T) requestAttributes.getAttribute(name, RequestAttributes.SCOPE_REQUEST);
    }

    private String translateErrorMessage(String message, Object[] arguments) {
        return messageSourceAccessor.getMessage(message, arguments);
    }

    private static class Error {

        @JsonProperty("error")
        private ErrorCode errorCode;
        private String message;

        // TODO: add some way to specify error details

        public ErrorCode getErrorCode() {
            return errorCode;
        }

        public String getMessage() {
            return message;
        }

    }

}
