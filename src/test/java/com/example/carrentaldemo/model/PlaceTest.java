package com.example.carrentaldemo.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.Validator;
import java.util.Set;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlaceTest {

    @Autowired
    private Validator validator;

    @Test
    public void should_validate_correctly() {
        Place place = new Place(UUID.randomUUID(), "Valid address");
        Set violations = validator.validate(place);
        Assert.assertTrue(violations.isEmpty());
    }

    @Test
    public void null_address_should_not_pass() {
        Place place = new Place(UUID.randomUUID(), null);
        Set violations = validator.validate(place);
        Assert.assertFalse(violations.isEmpty());
    }

    @Test
    public void empty_address_should_not_pass() {
        Place place = new Place(UUID.randomUUID(), "");
        Set violations = validator.validate(place);
        Assert.assertFalse(violations.isEmpty());
    }

}