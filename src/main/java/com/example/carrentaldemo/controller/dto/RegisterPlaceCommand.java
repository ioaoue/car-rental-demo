package com.example.carrentaldemo.controller.dto;

import javax.validation.constraints.NotBlank;

public class RegisterPlaceCommand {

    @NotBlank
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
