package com.example.carrentaldemo.controller.dto;

import java.util.UUID;

public class CarDto {

    private UUID id;
    private UUID modelId;
    private UUID currentPlaceId;
    private String number;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getModelId() {
        return modelId;
    }

    public void setModelId(UUID modelId) {
        this.modelId = modelId;
    }

    public UUID getCurrentPlaceId() {
        return currentPlaceId;
    }

    public void setCurrentPlaceId(UUID currentPlaceId) {
        this.currentPlaceId = currentPlaceId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
