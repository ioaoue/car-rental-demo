package com.example.carrentaldemo.controller.exception;

public class RestException extends RuntimeException {

    private ErrorCode errorCode;
    private Object[] arguments;

    public RestException(ErrorCode errorCode, String message, Object... arguments) {
        super(message);
        this.errorCode = errorCode;
        this.arguments = arguments;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public Object[] getArguments() {
        return arguments;
    }

}
