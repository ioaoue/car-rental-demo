package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Manufacturer;
import com.example.carrentaldemo.model.Model;
import com.example.carrentaldemo.model.QModel;
import com.example.carrentaldemo.repository.ModelRepository;
import com.example.carrentaldemo.service.exception.ManufacturerNotFoundException;
import com.example.carrentaldemo.service.exception.ModelNotFoundException;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class ModelServiceImpl implements ModelService {

    private ModelRepository modelRepository;
    private ManufacturerService manufacturerService;

    public ModelServiceImpl(ModelRepository modelRepository, ManufacturerService manufacturerService) {
        this.modelRepository = modelRepository;
        this.manufacturerService = manufacturerService;
    }

    @Transactional
    @Override
    public Model registerModel(String name, UUID manufacturerId) throws ManufacturerNotFoundException {
        Manufacturer manufacturer = manufacturerService.findActiveManufacturer(manufacturerId);
        Model model = new Model(UUID.randomUUID(), name, manufacturer);
        return modelRepository.save(model);
    }

    @Transactional(readOnly = true)
    @Override
    public Model findActiveModel(UUID modelId) throws ModelNotFoundException {
        Predicate predicate = modelIdEquals(modelId).and(modelIsActive());
        return modelRepository.findOne(predicate)
                .orElseThrow(() -> new ModelNotFoundException(modelId));
    }

    @Override
    public Iterable<Model> findActiveModels() {
        return modelRepository.findAll(modelIsActive());
    }

    private BooleanExpression modelIdEquals(UUID modelId) {
        return QModel.model.id.eq(modelId);
    }

    private BooleanExpression modelIsActive() {
        return QModel.model.active.isTrue();
    }

}
