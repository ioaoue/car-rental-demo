package com.example.carrentaldemo.controller.dto;

import com.example.carrentaldemo.model.RentStatus;

import java.time.Instant;
import java.util.UUID;

public class RentDto {

    private UUID id;
    private Instant rentTimestamp;
    private UUID rentPlaceId;
    private Instant returnTimestamp;
    private UUID returnPlaceId;
    private UUID carId;
    private UUID clientId;
    private RentStatus status;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Instant getRentTimestamp() {
        return rentTimestamp;
    }

    public void setRentTimestamp(Instant rentTimestamp) {
        this.rentTimestamp = rentTimestamp;
    }

    public UUID getRentPlaceId() {
        return rentPlaceId;
    }

    public void setRentPlaceId(UUID rentPlaceId) {
        this.rentPlaceId = rentPlaceId;
    }

    public Instant getReturnTimestamp() {
        return returnTimestamp;
    }

    public void setReturnTimestamp(Instant returnTimestamp) {
        this.returnTimestamp = returnTimestamp;
    }

    public UUID getReturnPlaceId() {
        return returnPlaceId;
    }

    public void setReturnPlaceId(UUID returnPlaceId) {
        this.returnPlaceId = returnPlaceId;
    }

    public UUID getCarId() {
        return carId;
    }

    public void setCarId(UUID carId) {
        this.carId = carId;
    }

    public UUID getClientId() {
        return clientId;
    }

    public void setClientId(UUID clientId) {
        this.clientId = clientId;
    }

    public RentStatus getStatus() {
        return status;
    }

    public void setStatus(RentStatus status) {
        this.status = status;
    }

}
