package com.example.carrentaldemo.controller;

import com.example.carrentaldemo.controller.dto.CarDto;
import com.example.carrentaldemo.controller.dto.DtoMapper;
import com.example.carrentaldemo.controller.dto.RegisterCarCommand;
import com.example.carrentaldemo.controller.dto.Response;
import com.example.carrentaldemo.controller.exception.ErrorCode;
import com.example.carrentaldemo.controller.exception.RestException;
import com.example.carrentaldemo.model.Car;
import com.example.carrentaldemo.service.CarService;
import com.example.carrentaldemo.service.exception.CarNotFoundException;
import com.example.carrentaldemo.service.exception.ModelNotFoundException;
import com.example.carrentaldemo.service.exception.PlaceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/rest/v1/cars")
public class CarController {

    private CarService carService;
    private DtoMapper dtoMapper;

    public CarController(CarService carService, DtoMapper dtoMapper) {
        this.carService = carService;
        this.dtoMapper = dtoMapper;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Response<CarDto>> registerCar(
            @RequestBody @Valid RegisterCarCommand cmd,
            UriComponentsBuilder uriBuilder
    ) {
        Car car;
        try {
            car = carService.registerCar(cmd.getNumber(), cmd.getModelId(), cmd.getCurrentPlaceId());
        } catch (ModelNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.model.notFound", e.getModelId());
        } catch (PlaceNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.place.notFound", e.getPlaceId());
        }
        CarDto carDto = dtoMapper.mapCar(car);

        URI location = uriBuilder.path("/rest/v1/cars/{id}")
                .build(carDto.getId());

        return ResponseEntity.created(location)
                .body(Response.success(carDto));
    }

    @GetMapping(value = "/{carId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<CarDto> getCar(@PathVariable UUID carId) {
        Car car;
        try {
            car = carService.findActiveCar(carId);
        } catch (CarNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.car.notFound", e.getCarId());
        }
        CarDto carDto = dtoMapper.mapCar(car);
        return Response.success(carDto);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Iterable<CarDto>> getCars() {
        Iterable<Car> cars = carService.findActiveCars();
        return Response.success(dtoMapper.mapCars(cars));
    }

}
