package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Car;
import com.example.carrentaldemo.model.Model;
import com.example.carrentaldemo.model.Place;
import com.example.carrentaldemo.model.QCar;
import com.example.carrentaldemo.repository.CarRepository;
import com.example.carrentaldemo.service.exception.CarNotFoundException;
import com.example.carrentaldemo.service.exception.ModelNotFoundException;
import com.example.carrentaldemo.service.exception.PlaceNotFoundException;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class CarServiceImpl implements CarService {

    private CarRepository carRepository;
    private ModelService modelService;
    private PlaceService placeService;

    public CarServiceImpl(CarRepository carRepository, ModelService modelService, PlaceService placeService) {
        this.carRepository = carRepository;
        this.modelService = modelService;
        this.placeService = placeService;
    }

    @Transactional
    @Override
    public Car registerCar(String number, UUID modelId, UUID placeId)
            throws ModelNotFoundException, PlaceNotFoundException {
        Model model = modelService.findActiveModel(modelId);
        Place place = placeService.findActivePlace(placeId);
        Car car = new Car(UUID.randomUUID(), model, place, number);
        return carRepository.save(car);
    }

    @Transactional(readOnly = true)
    @Override
    public Car findActiveCar(UUID carId) throws CarNotFoundException {
        Predicate predicate = carIdEquals(carId).and(carIsActive());
        return carRepository.findOne(predicate)
                .orElseThrow(() -> new CarNotFoundException(carId));
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<Car> findActiveCars() {
        return carRepository.findAll(carIsActive());
    }

    private BooleanExpression carIdEquals(UUID carId) {
        return QCar.car.id.eq(carId);
    }

    private BooleanExpression carIsActive() {
        return QCar.car.active.isTrue();
    }

}
