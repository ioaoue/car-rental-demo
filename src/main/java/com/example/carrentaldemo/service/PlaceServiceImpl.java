package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Place;
import com.example.carrentaldemo.model.QPlace;
import com.example.carrentaldemo.repository.PlaceRepository;
import com.example.carrentaldemo.service.exception.PlaceNotFoundException;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class PlaceServiceImpl implements PlaceService {

    private final PlaceRepository placeRepository;

    public PlaceServiceImpl(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    @Transactional
    @Override
    public Place registerPlace(String address) {
        Place place = new Place(UUID.randomUUID(), address);
        return placeRepository.save(place);
    }

    @Transactional(readOnly = true)
    @Override
    public Place findActivePlace(UUID placeId) throws PlaceNotFoundException {
        Predicate predicate = placeIdEquals(placeId).and(placeIsActive());
        return placeRepository.findOne(predicate)
                .orElseThrow(() -> new PlaceNotFoundException(placeId));
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<Place> findActivePlaces() {
        return placeRepository.findAll(placeIsActive());
    }

    private BooleanExpression placeIdEquals(UUID placeId) {
        return QPlace.place.id.eq(placeId);
    }

    private BooleanExpression placeIsActive() {
        return QPlace.place.active.isTrue();
    }

}
