package com.example.carrentaldemo.controller;

import com.example.carrentaldemo.controller.dto.DtoMapper;
import com.example.carrentaldemo.controller.dto.PlaceDto;
import com.example.carrentaldemo.controller.dto.RegisterPlaceCommand;
import com.example.carrentaldemo.controller.dto.Response;
import com.example.carrentaldemo.controller.exception.ErrorCode;
import com.example.carrentaldemo.controller.exception.RestException;
import com.example.carrentaldemo.model.Place;
import com.example.carrentaldemo.service.PlaceService;
import com.example.carrentaldemo.service.exception.PlaceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/rest/v1/places")
public class PlaceController {

    private PlaceService placeService;
    private DtoMapper dtoMapper;

    public PlaceController(PlaceService placeService, DtoMapper dtoMapper) {
        this.placeService = placeService;
        this.dtoMapper = dtoMapper;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Response<PlaceDto>> registerPlace(
            @RequestBody @Valid RegisterPlaceCommand cmd,
            UriComponentsBuilder uriBuilder
    ) {
        Place place = placeService.registerPlace(cmd.getAddress());
        PlaceDto placeDto = dtoMapper.mapPlace(place);

        URI location = uriBuilder.path("/rest/v1/places/{id}")
                .build(placeDto.getId());

        return ResponseEntity.created(location)
                .body(Response.success(placeDto));
    }

    @GetMapping(value = "/{placeId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Response<PlaceDto> getPlace(@PathVariable UUID placeId) {
        Place place;
        try {
            place = placeService.findActivePlace(placeId);
        } catch (PlaceNotFoundException e) {
            throw new RestException(ErrorCode.NOT_FOUND, "error.place.notFound", e.getPlaceId());
        }
        PlaceDto placeDto = dtoMapper.mapPlace(place);
        return Response.success(placeDto);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    Response<Iterable<PlaceDto>> getPlaces() {
        Iterable<Place> places = placeService.findActivePlaces();
        return Response.success(dtoMapper.mapPlaces(places));
    }

}
