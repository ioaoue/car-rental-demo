package com.example.carrentaldemo.controller.exception;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ErrorCode {

    NOT_FOUND("NotFound"),
    NOT_AVAILABLE("NotAvailable"),
    CLIENT("ClientError"),
    SERVER("ServerFault");

    private String code;

    ErrorCode(String code) {
        this.code = code;
    }

    @JsonValue
    public String getCode() {
        return code;
    }

}
