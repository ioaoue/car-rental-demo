package com.example.carrentaldemo.model;

import com.querydsl.core.annotations.QueryInit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Rent {
    @Id
    @NotNull
    private UUID id;

    @NotNull
    private Instant rentTimestamp;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private Place rentPlace;

    private Instant returnTimestamp;

    @ManyToOne(fetch = FetchType.LAZY)
    private Place returnPlace;

    @QueryInit("model.manufacturer")
    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private Car car;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private Client client;

    @Enumerated(EnumType.STRING)
    @NotNull
    private RentStatus status;

    @Version
    private long version;

    public Rent() {
    }

    public Rent(@NotNull UUID id, @NotNull Instant rentTimestamp, @NotNull Place rentPlace, @NotNull Car car, @NotNull Client client) {
        this.id = id;
        this.rentTimestamp = rentTimestamp;
        this.rentPlace = rentPlace;
        this.car = car;
        this.client = client;
        this.status = RentStatus.ACTIVE;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Instant getRentTimestamp() {
        return rentTimestamp;
    }

    public void setRentTimestamp(Instant rentTimestamp) {
        this.rentTimestamp = rentTimestamp;
    }

    public Place getRentPlace() {
        return rentPlace;
    }

    public void setRentPlace(Place rentPlace) {
        this.rentPlace = rentPlace;
    }

    public Instant getReturnTimestamp() {
        return returnTimestamp;
    }

    public void setReturnTimestamp(Instant returnTimestamp) {
        this.returnTimestamp = returnTimestamp;
    }

    public Place getReturnPlace() {
        return returnPlace;
    }

    public void setReturnPlace(Place returnPlace) {
        this.returnPlace = returnPlace;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public RentStatus getStatus() {
        return status;
    }

    public void setStatus(RentStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rent)) return false;

        Rent rent = (Rent) o;

        return Objects.equals(id, rent.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
