package com.example.carrentaldemo.service.exception;

import java.util.UUID;

public class CarNotRentedException extends Exception {

    private UUID carId;

    public CarNotRentedException(UUID carId) {
        super("Car was not rented: " + carId);
        this.carId = carId;
    }

    public UUID getCarId() {
        return carId;
    }

}