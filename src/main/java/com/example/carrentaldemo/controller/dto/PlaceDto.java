package com.example.carrentaldemo.controller.dto;

import java.util.UUID;

public class PlaceDto {

    private UUID id;
    private String address;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
