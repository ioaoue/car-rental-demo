package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Place;
import com.example.carrentaldemo.repository.PlaceRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PlaceServiceTest {

    @TestConfiguration
    static class PlaceServiceTestConfiguration {

        @MockBean
        private PlaceRepository placeRepository;

        @Bean
        PlaceService placeService(PlaceRepository placeRepository) {
            return new PlaceServiceImpl(placeRepository);
        }

    }

    @Autowired
    private PlaceService placeService;

    @Autowired
    private PlaceRepository placeRepository;

    @Test
    public void should_register_new_place() {
        Place place = new Place(UUID.randomUUID(), "Valid address");
        when(placeRepository.save(any())).thenReturn(place);
        Place newPlace = placeService.registerPlace(place.getAddress());
        assertEquals(place.getId(), newPlace.getId());
        assertEquals(place.getAddress(), newPlace.getAddress());
    }
}