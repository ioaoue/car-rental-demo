package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.Client;
import com.example.carrentaldemo.model.QClient;
import com.example.carrentaldemo.repository.ClientRepository;
import com.example.carrentaldemo.service.exception.ClientNotFoundException;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Transactional
    @Override
    public Client registerClient(String fullName) {
        Client client = new Client(UUID.randomUUID(), fullName);
        return clientRepository.save(client);
    }

    @Transactional(readOnly = true)
    @Override
    public Client findActiveClient(UUID clientId) throws ClientNotFoundException {
        Predicate predicate = clientIdEquals(clientId).and(clientIsActive());
        return clientRepository.findOne(predicate)
                .orElseThrow(() -> new ClientNotFoundException(clientId));
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<Client> findActiveClients() {
        return clientRepository.findAll(clientIsActive());
    }

    private BooleanExpression clientIdEquals(UUID clientId) {
        return QClient.client.id.eq(clientId);
    }

    private BooleanExpression clientIsActive() {
        return QClient.client.active.isTrue();
    }

}
