package com.example.carrentaldemo.service.exception;

import java.util.UUID;

public class CarNotFoundException extends Exception {

    private UUID carId;

    public CarNotFoundException(UUID carId) {
        super("Car was not found: " + carId);
        this.carId = carId;
    }

    public UUID getCarId() {
        return carId;
    }

}