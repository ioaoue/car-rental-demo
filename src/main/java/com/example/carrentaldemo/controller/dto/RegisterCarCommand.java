package com.example.carrentaldemo.controller.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public class RegisterCarCommand {

    @NotNull
    private UUID modelId;

    @NotNull
    private UUID currentPlaceId;

    @NotBlank
    private String number;

    public UUID getModelId() {
        return modelId;
    }

    public void setModelId(UUID modelId) {
        this.modelId = modelId;
    }

    public UUID getCurrentPlaceId() {
        return currentPlaceId;
    }

    public void setCurrentPlaceId(UUID currentPlaceId) {
        this.currentPlaceId = currentPlaceId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
