package com.example.carrentaldemo.controller.deserializer;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.time.Instant;

@Component
public class InstantConverter implements Converter<String, Instant> {

    @Override
    public Instant convert(@Nullable String source) {
        if (source == null) {
            return null;
        }
        long milliseconds = Long.parseLong(source);
        return Instant.ofEpochMilli(milliseconds);
    }

}
