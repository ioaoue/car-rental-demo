package com.example.carrentaldemo.controller.dto;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class RentCarCommand {

    @NotNull
    private UUID carId;

    @NotNull
    private UUID clientId;

    public UUID getCarId() {
        return carId;
    }

    public void setCarId(UUID carId) {
        this.carId = carId;
    }

    public UUID getClientId() {
        return clientId;
    }

    public void setClientId(UUID clientId) {
        this.clientId = clientId;
    }

}
