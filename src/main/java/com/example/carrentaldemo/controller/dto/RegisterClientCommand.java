package com.example.carrentaldemo.controller.dto;

import javax.validation.constraints.NotBlank;

public class RegisterClientCommand {

    @NotBlank
    private String fullName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

}
