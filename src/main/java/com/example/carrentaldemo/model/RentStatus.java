package com.example.carrentaldemo.model;

public enum RentStatus {
    ACTIVE,
    COMPLETED,
    CANCELLED
}
