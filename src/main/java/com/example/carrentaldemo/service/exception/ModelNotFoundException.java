package com.example.carrentaldemo.service.exception;

import java.util.UUID;

public class ModelNotFoundException extends Exception {

    private UUID modelId;

    public ModelNotFoundException(UUID modelId) {
        super("ModelDto was not found: " + modelId);
        this.modelId = modelId;
    }

    public UUID getModelId() {
        return modelId;
    }

}