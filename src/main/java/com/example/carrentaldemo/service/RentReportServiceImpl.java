package com.example.carrentaldemo.service;

import com.example.carrentaldemo.model.*;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.dsl.DateTimeExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RentReportServiceImpl implements RentReportService {

    private EntityManager entityManager;

    public RentReportServiceImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<AverageRentDuration> getAverageRentDuration(Instant rentTimestampMin, Instant rentTimestampMax) {
        QRent rent = QRent.rent;
        QManufacturer manufacturer = rent.car.model.manufacturer;
        QPlace place = rent.rentPlace;
        NumberExpression<Double> duration = toMilliseconds(rent.returnTimestamp)
                .subtract(toMilliseconds(rent.rentTimestamp))
                .avg();
        Iterable<Tuple> tuples = new JPAQuery<Tuple>(entityManager)
                .select(manufacturer, place, duration)
                .from(rent)
                .where(rent.status.eq(RentStatus.COMPLETED))
                .groupBy(manufacturer, place)
                .fetch();
        List<AverageRentDuration> result = new ArrayList<>();
        for (Tuple tuple : tuples) {
            AverageRentDuration average = new AverageRentDuration(
                    tuple.get(manufacturer),
                    tuple.get(place),
                    millisToDuration(tuple.get(duration))
            );
            result.add(average);
        }
        return result;
    }

    private NumberExpression<Long> toMilliseconds(DateTimeExpression<Instant> expression) {
        return Expressions.numberTemplate(Long.class, "function('timestamp_to_milliseconds', {0})", expression);
    }

    private Duration millisToDuration(@Nullable Double seconds) {
        return seconds == null ? null : Duration.ofMillis(seconds.longValue());
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<Rent> getHistory(
            UUID manufacturerId,
            Instant rentTimestampMin,
            Instant rentTimestampMax,
            Instant returnTimestampMin,
            Instant returnTimestampMax,
            String clientFullName,
            String carNumber
    ) {
        QRent rent = QRent.rent;
        QCar car = QCar.car;
        QClient client = QClient.client;
        JPAQuery<Rent> query = new JPAQuery<Rent>(entityManager)
                .from(rent)
                .innerJoin(rent.car, car).fetchJoin()
                .innerJoin(rent.client, client).fetchJoin();
        if (manufacturerId != null) {
            query = query.where(car.model.manufacturer.id.eq(manufacturerId));
        }
        if (rentTimestampMin != null) {
            query = query.where(rent.rentTimestamp.goe(rentTimestampMin));
        }
        if (rentTimestampMax != null) {
            query = query.where(rent.rentTimestamp.lt(rentTimestampMax));
        }
        if (returnTimestampMin != null) {
            query = query.where(rent.returnTimestamp.goe(returnTimestampMin));
        }
        if (returnTimestampMax != null) {
            query = query.where(rent.returnTimestamp.lt(returnTimestampMax));
        }
        if (clientFullName != null) {
            query = query.where(rent.client.fullName.likeIgnoreCase(clientFullName));
        }
        if (carNumber != null) {
            query = query.where(car.number.equalsIgnoreCase(carNumber));
        }
        return query.orderBy(rent.rentTimestamp.asc()).fetch();
    }

}
