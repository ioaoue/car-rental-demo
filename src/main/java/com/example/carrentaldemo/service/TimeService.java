package com.example.carrentaldemo.service;

import java.time.Instant;

public interface TimeService {

    Instant now();

}
