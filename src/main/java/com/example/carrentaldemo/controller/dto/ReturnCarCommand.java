package com.example.carrentaldemo.controller.dto;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class ReturnCarCommand {

    @NotNull
    private UUID carId;

    @NotNull
    private UUID placeId;

    public UUID getCarId() {
        return carId;
    }

    public void setCarId(UUID carId) {
        this.carId = carId;
    }

    public UUID getPlaceId() {
        return placeId;
    }

    public void setPlaceId(UUID placeId) {
        this.placeId = placeId;
    }

}
