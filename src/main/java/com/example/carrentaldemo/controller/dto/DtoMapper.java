package com.example.carrentaldemo.controller.dto;

import com.example.carrentaldemo.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface DtoMapper {

    @Mapping(source = "model.id", target = "modelId")
    @Mapping(source = "currentPlace.id", target = "currentPlaceId")
    CarDto mapCar(Car source);
    Iterable<CarDto> mapCars(Iterable<Car> source);

    ClientDto mapClient(Client source);
    Iterable<ClientDto> mapClients(Iterable<Client> source);

    ManufacturerDto mapManufacturer(Manufacturer source);
    Iterable<ManufacturerDto> mapManufacturers(Iterable<Manufacturer> source);

    @Mapping(source = "manufacturer.id", target = "manufacturerId")
    ModelDto mapModel(Model source);
    Iterable<ModelDto> mapModels(Iterable<Model> source);

    PlaceDto mapPlace(Place source);
    Iterable<PlaceDto> mapPlaces(Iterable<Place> source);

    @Mapping(source = "rentPlace.id", target = "rentPlaceId")
    @Mapping(source = "returnPlace.id", target = "returnPlaceId")
    @Mapping(source = "car.id", target = "carId")
    @Mapping(source = "client.id", target = "clientId")
    RentDto mapRent(Rent source);
    Iterable<RentDto> mapRents(Iterable<Rent> source);

    @Mapping(source = "client.fullName", target = "clientFullName")
    @Mapping(source = "car.number", target = "carNumber")
    RentHistoryDto mapRentHistory(Rent source);
    Iterable<RentHistoryDto> mapRentHistory(Iterable<Rent> source);

}
