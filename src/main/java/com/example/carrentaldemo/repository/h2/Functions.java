package com.example.carrentaldemo.repository.h2;

import java.sql.Timestamp;

public class Functions {

    public static Long timestampToMilliseconds(Timestamp timestamp) {
        return timestamp.toInstant().toEpochMilli();
    }

}
